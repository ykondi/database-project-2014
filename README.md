2014 CC 105 Database Systems Lab Project
========================================

Authors
-------
* Ananth Murthy
* Chandan Yeshwanth
* Karthik S
* Yashvanth Kondi

File Descriptions
-----------------
* **use_cases:** Describes operations and attributes of the entities and
actors.
* **use_cases_dao:** High Level DAO design.
* **TODO:** List of items to do.
* **naukri_model.mwb:** MySQL Workbench EER Model
* **naukri.sql** SQL Script forward engineered from above model
* **src/x.java** Application class
* **src/xDAO.java** Data access objects
* **src/xDAO_JDBC.java** JDBC class
