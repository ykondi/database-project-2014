-- copied from generated .sql.
-- primary indices are here
-- FKs are in alter.sql

-- -----------------------------------------------------
-- Table `trainer`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `trainer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skill`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `skill` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `course`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `course` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `skill` INT NOT NULL,
  `trainerid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Course_Trainer_idx` (`trainerid` ASC),
  INDEX `fk_course_skill1_idx` (`skill` ASC)
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `applicant`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `applicant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `qualification` VARCHAR(45) NULL,
  `personaldata` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `company`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `company` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joboffer`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `joboffer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `basesalary` INT NULL,
  `companyid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_JobOffer_Company1_idx` (`companyid` ASC)
  )  
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `applicant_course`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `applicant_course` (
  `applicantid` INT NOT NULL,
  `courseid` INT NOT NULL,
  PRIMARY KEY (`applicantid`, `courseid`),
  INDEX `fk_Applicant_has_Course_Course1_idx` (`courseid` ASC),
  INDEX `fk_Applicant_has_Course_Applicant1_idx` (`applicantid` ASC)
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `applicant_joboffer`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `applicant_joboffer` (
  `applicantid` INT NOT NULL,
  `jobofferid` INT NOT NULL,
  PRIMARY KEY (`applicantid`, `jobofferid`),
  INDEX `fk_Applicant_has_JobOffer_JobOffer1_idx` (`jobofferid` ASC),
  INDEX `fk_Applicant_has_JobOffer_Applicant1_idx` (`applicantid` ASC) 
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `applicant_skill`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `applicant_skill` (
  `applicantid` INT NOT NULL,
  `skillid` INT NOT NULL,
  PRIMARY KEY (`applicantid`, `skillid`),
  INDEX `fk_Applicant_has_Skill_Skill1_idx` (`skillid` ASC),
  INDEX `fk_Applicant_has_Skill_Applicant1_idx` (`applicantid` ASC)
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `joboffer_skill`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `joboffer_skill` (
  `jobofferid` INT NOT NULL,
  `skillid` INT NOT NULL,
  PRIMARY KEY (`jobofferid`, `skillid`),
  INDEX `fk_JobOffer_has_Skill_Skill1_idx` (`skillid` ASC),
  INDEX `fk_JobOffer_has_Skill_JobOffer1_idx` (`jobofferid` ASC)  
  )
ENGINE = InnoDB;
