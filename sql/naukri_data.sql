insert into skill values (1, "java"),
						 (2, "c"),
						 (3, "python"),
						 (4, "c#");

insert into company values (1, "microsoft"), 
						   (2, "google"), 
						   (3, "facebook");		

insert into trainer(name) values ("karthik"), ("reijul"), ("abc");

insert into course(name, skill, trainerid) values ("AB101", 1, 1), ("DC106", 3, 2), ("Radix", 1, 3);