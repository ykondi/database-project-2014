import java.lang.*;
import java.util.ArrayList;

public class Applicant{
	int ID;
	String name;
	String qualification;
	String personaldata;
	ArrayList<Skill> skills;

	
	public Applicant(int _id, String _name, String _qualification, String _personaldata, ArrayList<Skill> _skills) {
		ID = _id;
		name = _name;
		qualification = _qualification;
		personaldata = _personaldata;
		skills = _skills;
	 }
	public Applicant (){  }
	
	public int getId() { return ID; }
	public void setId(int s){ ID = s; }
	
	public String getName() { return name; }
	public void setName(String s){ name = s; }
	
	public String getQualification() { return qualification; }
	public void setQualification(String s){ qualification = s; }
	
	public String getPersonaldata() { return personaldata; }
	public void setPersonaldata(String s){ personaldata = s; }

	public ArrayList<Skill> getSkills(){ return skills; }
	public void setSkills(ArrayList<Skill> _skills) { skills = _skills; }
	
	public void print(){ 
		System.out.println("Applicant ID=" + ID); 
		System.out.println("Name=" + name);
		System.out.println("Qualification=" + qualification);
		System.out.println("Personal Data=" + personaldata);
	}
};

