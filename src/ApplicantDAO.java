import java.lang.*;
import java.util.List;

public interface ApplicantDAO {
	public void addApplicant(Applicant applicant) throws Exception;
	public void applyForJobOffer(int applicantID, int jobId)throws Exception;
	public void enrolInCourse(int applicantId, int courseId)throws Exception;
}

