import java.util.ArrayList;
import java.util.List;
import java.sql.*;


public class CourseDAO_JDBC implements CourseDAO {

Connection dbConnection;

	public CourseDAO_JDBC(Connection dbconn){
		// JDBC driver name and database URL
 		//  Database credentials
		dbConnection = dbconn;
	}

	@Override
	public ArrayList<Course> browseCourseBySkill(String skill_name) throws Exception {
		ArrayList<Course> c = new ArrayList<Course>();
		String sql, sql2, sql3;
		Statement stmt = null;
		
		try{
			stmt = dbConnection.createStatement();
			sql = "select c.name, c.id, c.trainerid, c.skill from course c, skill s where s.name=\"" + skill_name + "\" and c.skill=s.id";
			sql2 = "select * from trainer where id = ";
			sql3 = "select * from skill where id = ";
			System.out.println("executing: " + sql);
			
			ResultSet rs = stmt.executeQuery(sql);
			ResultSet rs2, rs3;

			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				String course_name = rs.getString("c.name");
				int skill_id = rs.getInt("c.skill");
				int tid = rs.getInt("c.trainerid");
				int id = rs.getInt("c.id");

				Course temp = new Course();

				temp.setName(course_name);
				temp.setSkill(null);
				temp.setTrainer(null);
				temp.setID(id);

				stmt = dbConnection.createStatement();
				rs2 = stmt.executeQuery(sql2 + tid);
				rs2.next();

				Trainer trainer = new Trainer();
				trainer.setID(tid);
				trainer.setName(rs2.getString("name"));

				stmt = dbConnection.createStatement();
				rs3 = stmt.executeQuery(sql2 + skill_id);
				rs3.next();

				Skill skill = new Skill(rs3.getInt("id"), rs3.getString("name"));
				
				temp.setTrainer(trainer);
				temp.setSkill(skill);

				c.add(temp);
			}
		} catch (SQLException ex) {
		    // handle any errors
		    // System.out.println("SQLException: " + ex.getMessage());
		    // System.out.println("SQLState: " + ex.getSQLState());
		    // System.out.println("VendorError: " + ex.getErrorCode());
		    System.out.println("Could not find the course you are looking for.");
		    throw ex;
		}
		// Add exception handling when there is no matching record
		return c;
	}

	@Override
	public ArrayList<Course> browseCourseBySkill(int skill_id) throws Exception {
		ArrayList<Course> c = new ArrayList<Course>();
		String sql, sql2, sql3;
		Statement stmt = null;
		
		try{
			stmt = dbConnection.createStatement();
			sql = "select c.name, c.id, c.trainerid from course c where c.skill=" + skill_id;
			sql2 = "select * from trainer where id = ";
			sql3 = "select * from skill where id = ";
			ResultSet rs = stmt.executeQuery(sql);
			ResultSet rs2, rs3;
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				String course_name = rs.getString("c.name");
				int tid = rs.getInt("c.trainerid");
				int id = rs.getInt("c.id");

				Course temp = new Course();

				stmt = dbConnection.createStatement();
				rs2 = stmt.executeQuery(sql2 + tid);
				Trainer trainer = new Trainer();
				trainer.setID(tid);
				trainer.setName(rs2.getString("name"));

				stmt = dbConnection.createStatement();
				rs3 = stmt.executeQuery(sql2 + skill_id);

				Skill skill = new Skill(rs3.getInt("id"), rs3.getString("name"));

				temp.setName(course_name);
				temp.setSkill(skill);
				temp.setTrainer(trainer);
				temp.setID(id);

				c.add(temp);
			}
		} catch (SQLException ex) {
		    // handle any errors
		    // System.out.println("SQLException: " + ex.getMessage());
		    // System.out.println("SQLState: " + ex.getSQLState());
		    // System.out.println("VendorError: " + ex.getErrorCode());
		    System.out.println("Could not find the course you are looking for.");
		    throw ex;
		}
		// Add exception handling when there is no matching record
		return c;
	}

	@Override
	public void addCourse(Course course) throws Exception {
		PreparedStatement preparedStatement = null;
		String sql;
		sql = "insert into course(name, skill, trainerid) values (?,?,?)";

		try {
			preparedStatement = dbConnection.prepareStatement(sql);
 
			//preparedStatement.setInt(1, course.getID());
			preparedStatement.setString(1, course.getName());
			preparedStatement.setInt(2, course.getSkill().getID());
			preparedStatement.setInt(3, course.getTrainer().getID());
 
			// execute insert SQL stetement
			preparedStatement.executeUpdate();
 
			// System.out.println("Course: ID " + course.getID() 
			// 	+ ", added to the database");
		} catch (SQLException e) {
 			// System.out.println(e.getMessage());
 			System.out.println("Failed to add course, check if the trainer and skills exist.");
		    throw e;
 		}

		try{
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException e) {
 			System.out.println(e.getMessage());
		    	throw e;
 		}
	}
}
