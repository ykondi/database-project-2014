import java.lang.*;
import java.util.*;

public interface SkillDAO 
{
	public ArrayList<Skill> getAllSkills() throws Exception;
}

