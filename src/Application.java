import java.sql.*;
import java.util.*;

public class Application {
	int userId;
	Scanner scanner;

	DAO_Factory daoFactory;
	JobOfferDAO jdao;
	ApplicantDAO adao;
	CourseDAO cdao;
	SkillDAO sdao;

	ArrayList<Skill> allSkills;

	public Application(){
		userId = -1;
		scanner = new Scanner(System.in);
	}

	public static void main(String args[]) throws Exception {
		Application app = new Application();
		app.run();
	}

	public void run() throws Exception {
		int choice;

		daoFactory = new DAO_Factory();
		getAllSkills();

		do {
			clear();

			System.out.println("\t\tNaukri.com\n");
			System.out.println("I am an -\n");
			System.out.println("1. Applicant");
			System.out.println("2. Trainer");
			System.out.println("3. Company");
			System.out.println("\n0. Exit");

			choice = scanner.nextInt();

			switch(choice){
				case 1: showApplicantMenu(); break;
				case 2: showTrainerMenu(); break;
				case 3: showCompanyMenu(); break;
				default: break;
			}
		} while( choice != 0 );
	}

	private void showApplicantMenu() throws Exception {
		int choice;

		do {
			clear();

			System.out.println("\t\tNaukri.com\n");
			System.out.println("1. Create a new Account");
			System.out.println("2. Login");
			System.out.println("\n0. Back to Main Menu");

			choice = scanner.nextInt();

			switch(choice){
				case 1: break;
				case 2: break;
				default: break;
			}
		} while( choice != 0 );
	}

	private void showTrainerMenu(){
		int choice;	
	}

	private void showCompanyMenu(){
		int choice;
	}

	private void getAllSkills()
	{
		try
		{
			daoFactory.activateConnection();
			sdao = daoFactory.getSkillDAO();
			allSkills = sdao.getAllSkills();
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
			
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}

	private void clear(){
		final String ANSI_CLS = "\u001b[2J";
        final String ANSI_HOME = "\u001b[H";
        System.out.print(ANSI_CLS + ANSI_HOME);
        System.out.flush();
	}
}