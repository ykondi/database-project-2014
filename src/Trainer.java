import java.lang.*;

public class Trainer{
	int ID;
	String name;
	
	public Trainer() { }
	
	public String getName() { return name; }
	public void setName(String s){ name = s; }
	
	public int getID() { return ID; }
	public void setID(int s){ ID = s; }
	
	public void print()
	{ 
		System.out.println("\tTrainer ID = " + ID); 
		System.out.println("\tName = " + name);
	}
};
