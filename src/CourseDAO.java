import java.lang.*;
import java.util.*;

public interface CourseDAO 
{
	public ArrayList<Course> browseCourseBySkill(String skill_name) throws Exception;
	public ArrayList<Course> browseCourseBySkill(int skill_id) throws Exception;
	public void addCourse(Course course) throws Exception;
}

