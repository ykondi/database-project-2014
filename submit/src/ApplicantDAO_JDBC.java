import java.util.ArrayList;
import java.util.List;
import java.sql.*;


public class ApplicantDAO_JDBC implements ApplicantDAO {
																																																																																																																																																																																																																																															Connection dbConnection;

	public ApplicantDAO_JDBC(Connection dbconn){
		// JDBC driver name and database URL
 		//  Database credentials
		dbConnection = dbconn;
	}
	

	@Override
	public void addApplicant(Applicant applicant) throws Exception
	 {
		PreparedStatement preparedStatement=null;																																																																																																																																													
		String sql, sql2;
		ResultSet rs;

		sql = "insert into applicant(name, qualification, personaldata) values (?, ?, ?)";
		sql2 = "insert into applicant_skill (applicantid, skillid) values (?, ?)";

		try {
			// needs key of newly inserted record
			preparedStatement = dbConnection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
 
			preparedStatement.setString(1, applicant.getName());
			preparedStatement.setString(2, applicant.getQualification());
			preparedStatement.setString(3, applicant.getPersonaldata());
 
			// execute insert 
			preparedStatement.executeUpdate();
			// get key
			rs = preparedStatement.getGeneratedKeys();

			// insert into applicant_skill table
			if(rs.next()){
				int applicantId = rs.getInt(1);	

				preparedStatement = dbConnection.prepareStatement(sql2);
				preparedStatement.setInt(1, applicantId);

				for(Skill skill : applicant.getSkills()){
					preparedStatement.setInt(2, skill.getID());
					preparedStatement.executeUpdate();
				}
			}

		} catch (SQLException ex) {
 			// System.out.println("SQLException: " + ex.getMessage());
		  //   System.out.println("SQLState: " + ex.getSQLState());
		  //   System.out.println("VendorError: " + ex.getErrorCode());
			System.out.println("Failed to register, try again.");
			throw ex;
 		}

		try{
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException e) {
 			System.out.println(e.getMessage());
			throw e;
 		}
	}
	
	@Override
	public void applyForJobOffer(int applicantID, int jobId) throws Exception{
		PreparedStatement preparedStatement = null;																																																																																																																																													
		String sql;
		sql = "insert into applicant_joboffer(applicantid, jobofferid) values (?,?)";

		try {
			preparedStatement = dbConnection.prepareStatement(sql);
 
			preparedStatement.setInt(1, applicantID);
			preparedStatement.setInt(2, jobId);
 
			// execute insert SQL stetement
			preparedStatement.executeUpdate();
 
			
		} catch (SQLException e) {
 			// System.out.println(e.getMessage());
 			System.out.println("Failed to apply, check if the job exists or if you have already applied.");
			throw e;
 		}

		try{
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException e) {
 			System.out.println(e.getMessage());
			throw e;
 		}
	}

	@Override
	public void enrolInCourse(int applicantID, int courseId) throws Exception{
		PreparedStatement preparedStatement = null;																																																																																																																																													
		String sql;
		sql = "insert into applicant_course(applicantid, courseid) values (?,?)";

		try {
			preparedStatement = dbConnection.prepareStatement(sql);
 
			preparedStatement.setInt(1, applicantID);
			preparedStatement.setInt(2, courseId);
 
			// execute insert SQL stetement
			preparedStatement.executeUpdate();
 
			
		} catch (SQLException e) {
 			// System.out.println(e.getMessage());
 			System.out.println("Failed to enrol, check if the course exists or if you have already enrolled.");
			throw e;
 		}

		try{
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException e) {
 			System.out.println(e.getMessage());
			throw e;
 		}
	}

}
