import java.lang.*;
import java.util.*;
import java.sql.*;

public class SkillDAO_JDBC implements SkillDAO
{
	Connection dbConnection;

	public SkillDAO_JDBC(Connection dbconn){
		dbConnection = dbconn;
	}

	public ArrayList<Skill> getAllSkills() throws Exception
	{
		ArrayList<Skill> skills = new ArrayList<Skill>();
		PreparedStatement pstmt = null;
		String sql = "select * from skill";
		ResultSet rs;
		Skill skill;

		try {
			// get skills
			pstmt = dbConnection.prepareStatement(sql);
			rs = pstmt.executeQuery();

			// add to list
			while(rs.next()) {
				skills.add(new Skill(rs.getInt("id"),rs.getString("name")));
			}
		}	
		catch(SQLException ex) {
			// System.out.println("SQLException: " + ex.getMessage());
		 //    System.out.println("SQLState: " + ex.getSQLState());
		 //    System.out.println("VendorError: " + ex.getErrorCode());
			System.out.println("Could not find skills.");
			throw ex;
		}
		
		return skills;		
	}
}

