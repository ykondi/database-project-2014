import java.lang.*;
import java.util.*;

public interface JobOfferDAO 
{
	public ArrayList<JobOffer> browseJobBySalary(int low) throws Exception;
	public void addJobOffer(JobOffer jobOffer) throws Exception;
	public void removeJobOffer(int ID) throws Exception;
}

