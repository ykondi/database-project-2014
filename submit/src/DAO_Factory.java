import java.lang.*;
import java.sql.*;
/*
	Methods to be called in the following order:

	1. activateConnection
	2. 	Any number getDAO calls with any number of database transactions
	3. deactivateConnection
*/
public class DAO_Factory{
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/naukri";
	static final String USER = "root";
	static final String PASS = "password123";	
	Connection dbconnection = null;
	
	public enum TXN_STATUS { COMMIT, ROLLBACK };
	// You can add additional DAOs here as needed
	ApplicantDAO applicantDAO = null;
	JobOfferDAO jobOfferDAO = null;
	CourseDAO courseDAO = null;
	SkillDAO skillDAO = null;

	boolean activeConnection = false;

	public DAO_Factory()
	{
		dbconnection = null;
		activeConnection = false;
	}

	public void activateConnection() throws Exception
	{
		if( activeConnection == true )
			throw new Exception("Connection already active");

		try{
			Class.forName("com.mysql.jdbc.Driver");
			dbconnection = DriverManager.getConnection(DB_URL, USER, PASS);
			dbconnection.setAutoCommit(false);
			activeConnection = true;
		} catch(ClassNotFoundException ex) {
			System.out.println("Error: unable to load driver class!");
			System.exit(1);
		} catch (SQLException ex) {
		    // handle any errors
		    // System.out.println("SQLException: " + ex.getMessage());
		    // System.out.println("SQLState: " + ex.getSQLState());
		    // System.out.println("VendorError: " + ex.getErrorCode());
		    System.out.println("Unable to connect, try again.");
		}
	}


	public ApplicantDAO getApplicantDAO() throws Exception
	{
		if( activeConnection == false )
			throw new Exception("Connection not activated.");

		if( applicantDAO == null )
			applicantDAO = new ApplicantDAO_JDBC( dbconnection );

		return applicantDAO;
	}


	public CourseDAO getCourseDAO() throws Exception
	{
		if( activeConnection == false )
			throw new Exception("Connection not activated.");

		if( courseDAO == null )
			courseDAO = new CourseDAO_JDBC( dbconnection );

		return courseDAO;
	}


	public JobOfferDAO getJobOfferDAO() throws Exception
	{
		if( activeConnection == false )
			throw new Exception("Connection not activated...");

		if( jobOfferDAO == null )
			jobOfferDAO = new JobOfferDAO_JDBC( dbconnection );

		return jobOfferDAO;
	}
	
	
	public SkillDAO getSkillDAO() throws Exception
	{
		if( activeConnection == false )
			throw new Exception("Connection not activated...");

		if( skillDAO == null )
			skillDAO = new SkillDAO_JDBC( dbconnection );

		return skillDAO;
	}

	// supports txns
	public void deactivateConnection( TXN_STATUS txn_status )
	{
		// Okay to keep deactivating an already deactivated connection
		activeConnection = false;
		if( dbconnection != null ){
			try{
				if( txn_status == TXN_STATUS.COMMIT )
					dbconnection.commit();
				else
					dbconnection.rollback();

				dbconnection.close();
				dbconnection = null;

				// Nullify all DAO objects
				applicantDAO = null;
				jobOfferDAO = null;
				courseDAO = null;
			}
			catch (SQLException ex) {
			    // handle any errors
			    // System.out.println("SQLException: " + ex.getMessage());
			    // System.out.println("SQLState: " + ex.getSQLState());
			    // System.out.println("VendorError: " + ex.getErrorCode());
			    System.out.println("Could not disconnect from database.");
			}
		}
	}
};

