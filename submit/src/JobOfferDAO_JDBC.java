import java.lang.*;
import java.util.*;
import java.sql.*;

public class JobOfferDAO_JDBC implements JobOfferDAO
{
	Connection dbConnection;

	// constructor
	public JobOfferDAO_JDBC(Connection dbconn){
		dbConnection = dbconn;
	}

	// returns jobs with basesalary > low
	public ArrayList<JobOffer> browseJobBySalary(int low) throws Exception
	{
		ArrayList<JobOffer> lst = new ArrayList<JobOffer>();
		ArrayList<Skill> skills = new ArrayList<Skill>();

		PreparedStatement pstmt = null,
						  pstmt2 = null;
		ResultSet rs, rs2;

		String sql1, sql2, sql3;
		int jobId;
		
		sql1 = "SELECT * from joboffer where basesalary > ?";
		sql2 = "SELECT s.id id, s.name name from joboffer_skill js, skill s where js.jobofferid=? and s.id=js.skillid";
		sql3 = "SELECT c.id id, c.name name from joboffer j, company c where j.id=? and j.companyid=c.id";

		try{
			pstmt = dbConnection.prepareStatement(sql1);
			pstmt.setInt(1, low);

			// fetch joboffers
			rs = pstmt.executeQuery();
			
			JobOffer temp;

			while(rs.next())
			{
				temp = new JobOffer();
				jobId = rs.getInt("id");

				temp.setId(jobId);
				temp.setBaseSalary(rs.getInt("basesalary"));
				
				// get skills for this job
				pstmt2 = dbConnection.prepareStatement(sql2);
				pstmt2.setInt(1, jobId);
				rs2 = pstmt2.executeQuery();

				// set all skills
				while(rs2.next()){
					skills.add(new Skill(
							rs2.getInt("id"),
							rs2.getString("name")
						));
				}

				temp.setSkills(skills);

				// get company for this job
				pstmt2 = dbConnection.prepareStatement(sql3);
				pstmt2.setInt(1, jobId);
				rs2 = pstmt2.executeQuery();
				rs2.next();

				// add company to joboffer
				temp.setCompany(new Company(
						rs2.getInt("id"),
						rs2.getString("name")
					));

				lst.add(temp);
			}
		}
		catch(SQLException ex){
			// System.out.println("SQLException: " + ex.getMessage());
		 //    System.out.println("SQLState: " + ex.getSQLState());
		 //    System.out.println("VendorError: " + ex.getErrorCode());
			   System.out.println("Could not find jobs.");
		    throw ex;
		}
		
		return lst;
	}

	// adds jobOffer to joboffer table 
	public void addJobOffer(JobOffer jobOffer) throws Exception{
		PreparedStatement pstmt = null;
		String sql1, sql2;
		ResultSet rs;
		int jobId;
		
		sql1 = "INSERT INTO joboffer (basesalary, companyid) VALUES (?, ?)";
		sql2 = "INSERT INTO joboffer_skill (jobofferid, skillid) VALUES (?, ?)";

		try{
			pstmt = dbConnection.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, jobOffer.getBaseSalary());
			pstmt.setInt(2, jobOffer.getCompany().getID());

			// insert joboffer
			pstmt.executeUpdate();

			// get jobid from newly inserted job
			// returnLastInsertId seems to be internal variable
			rs = pstmt.getGeneratedKeys();
			if(rs.next()) {
				// this should work\
				jobId = rs.getInt(1);	

				// insert skills
				pstmt = dbConnection.prepareStatement(sql2);
				pstmt.setInt(1, jobId);

				for(Skill skill : jobOffer.getSkills()){
					pstmt.setInt(2, skill.getID());
					pstmt.executeUpdate();
				}
			}
		}
		catch(SQLException ex){
			// System.out.println("SQLException: " + ex.getMessage());
		 //    System.out.println("SQLState: " + ex.getSQLState());
		 //    System.out.println("VendorError: " + ex.getErrorCode());
			System.out.println("Failed to add job, check if the company and skills exist.");
		    throw ex;
		}
	}

	// removes jobOffer from joboffer table
	public void removeJobOffer(int ID) throws Exception {
		PreparedStatement pstmt = null;
		String sql1, sql2, sql3;

		sql1 = "DELETE FROM joboffer WHERE id = ?";
		sql2 = "DELETE FROM joboffer_skill WHERE jobofferid = ?";
		sql3 = "DELETE FROM applicant_joboffer WHERE jobofferid = ?";

		try{
			// remove from joboffer_skill table
			pstmt = dbConnection.prepareStatement(sql2);
			pstmt.setInt(1, ID);
			pstmt.executeUpdate();

			// remove from applicant_joboffer table
			pstmt = dbConnection.prepareStatement(sql3);
			pstmt.setInt(1, ID);
			pstmt.executeUpdate();

			// remove from joboffer table
			pstmt = dbConnection.prepareStatement(sql1);
			pstmt.setInt(1, ID);
			pstmt.executeUpdate();
		}
		catch(SQLException ex){
			// System.out.println("SQLException: " + ex.getMessage());
		 //    System.out.println("SQLState: " + ex.getSQLState());
		 //    System.out.println("VendorError: " + ex.getErrorCode());
			System.out.println("Failed to remove course, check if it exists.");
		    throw ex;
		}
	}
	
}

