//STEP 1. Import required packages
import java.sql.*;
import java.util.*;


public class DAO_Demo {

	public static DAO_Factory daoFactory = null;
	public static JobOfferDAO jdao = null;
	public static ApplicantDAO adao = null;
	public static CourseDAO cdao = null;
	public static SkillDAO sdao = null;
	public static ArrayList<Skill> allSkills = null;
	
	public static void getAllSkills()
	{
		try
		{
			daoFactory.activateConnection();
			sdao = daoFactory.getSkillDAO();
			allSkills = sdao.getAllSkills();
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
			
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	public static void displayAllSkills()
	{
		System.out.println("\nList of All Skills\n");
		for(Skill skill : allSkills)
		{
			skill.print();
		}
		System.out.println("--------------------\n");
		// System.out.println("1. java\n2. c\n3. c#\n4. python");
	}
	
	public static void func1()
	{
		Scanner scanner = new Scanner(System.in);
		Applicant applicant = new Applicant();
		JobOffer jobOffer = new JobOffer();
		
		
		System.out.print(" Please enter Applicant's name : ");
		String str = scanner.next();
		applicant.setName(str);
		
		System.out.print(" Please enter Applicant's Qualifications : ");
		str = scanner.next();
		applicant.setQualification(str);		
		
		System.out.print(" Please enter Applicant's personal data : ");
		str = scanner.next();
		applicant.setPersonaldata(str);
		
		
		displayAllSkills();
		System.out.print(" Please enter number of skills : ");
		int n = scanner.nextInt();
		
		int k, i;
		Skill temp;
		ArrayList <Skill> skills = new ArrayList <Skill>(); 
		
		for(i = 0; i < n; i++)
		{
			temp = new Skill();
			System.out.print(" Please enter Skill ID : ");
			k = scanner.nextInt();
			temp.setID(k);
			skills.add(temp);
		}
		
		applicant.setSkills(skills);
		
		try
		{
			daoFactory.activateConnection();
			adao = daoFactory.getApplicantDAO();
			
			adao.addApplicant(applicant);
			
			System.out.println(" Successfully added the Applicant Profile. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void func2()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please enter minimum salary : ");
		int i = scanner.nextInt();

		try
		{
			daoFactory.activateConnection();
			jdao = daoFactory.getJobOfferDAO();
			if(jdao == null){ System.out.println("nullDAO"); }
			ArrayList<JobOffer> jobs = jdao.browseJobBySalary(i);

			for(JobOffer job : jobs)
			{
				job.print();
				System.out.println("--------------------\n");
			}
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void func3()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please enter Applicant ID : ");
		int i = scanner.nextInt();
		
		System.out.print(" Please enter Job ID : ");
		int j = scanner.nextInt();
		
		try
		{
			daoFactory.activateConnection();
			adao = daoFactory.getApplicantDAO();
			adao.applyForJobOffer(i, j);

			System.out.println(" Successfully applied for job. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	public static void func4()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please enter skill name : ");
		String skill = scanner.next ();
		int i;
		try
		{
			daoFactory.activateConnection();
			cdao = daoFactory.getCourseDAO();
			ArrayList<Course> courses = cdao.browseCourseBySkill(skill);
		
			for(i = 0; i < courses.size(); i++)
			{
				courses.get(i).print();
				System.out.println("--------------------\n");
			}
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		}
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
		
	
	public static void func5()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please enter Applicant ID : ");
		int i = scanner.nextInt();
		
		System.out.print(" Please enter Course ID : ");
		int j = scanner.nextInt();
		
		try
		{
			daoFactory.activateConnection();
			adao = daoFactory.getApplicantDAO();
			adao.enrolInCourse(i, j);

			System.out.println(" Successfully enrolled in course. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void func6()
	{
		Scanner scanner = new Scanner(System.in);
		Company company = new Company();
		JobOffer jobOffer = new JobOffer();
		
		
		System.out.print(" Please enter base salary : ");
		int i = scanner.nextInt();
		jobOffer.setBaseSalary(i);
		
		System.out.print(" Please enter Company ID : ");
		i = scanner.nextInt();
		company.setID(i);
		
		jobOffer.setCompany(company);
		
		displayAllSkills();

		System.out.print(" Please enter number of skills : ");
		int n = scanner.nextInt();
		
		int k;
		Skill temp;
		ArrayList <Skill> skills = new ArrayList <Skill>(); 
		
		for(i = 0; i < n; i++)
		{
			temp = new Skill();
			System.out.print(" Please enter Skill ID : ");
			k = scanner.nextInt();
			temp.setID(k);
			skills.add(temp);
		}
		
		jobOffer.setSkills(skills);
		
		try
		{
			daoFactory.activateConnection();
			jdao = daoFactory.getJobOfferDAO();
			
			jdao.addJobOffer(jobOffer);
			
			System.out.println(" Successfully added a Job Offer. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void func7()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please enter Job Offer ID : ");
		int i = scanner.nextInt();
		
		try
		{
			daoFactory.activateConnection();
			jdao = daoFactory.getJobOfferDAO();
			jdao.removeJobOffer(i);

			System.out.println(" Successfully withdrew Job Offer. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		}
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void func8()
	{
		Scanner scanner = new Scanner(System.in);
		Trainer trainer = new Trainer();
		Course course = new Course();
		Skill skill = new Skill();
		
		System.out.print(" Please enter Course name : ");
		String name = scanner.next();
		course.setName(name);
		
		displayAllSkills();
		System.out.print(" Please enter Skill ID : ");
		int i = scanner.nextInt();
		skill.setID(i);
		course.setSkill(skill);
		
		System.out.print(" Please enter Trainer ID : ");
		i = scanner.nextInt();
		trainer.setID(i);
		course.setTrainer(trainer);
		
		try
		{
			daoFactory.activateConnection();
			cdao = daoFactory.getCourseDAO();
			
			cdao.addCourse(course);
			
			System.out.println(" Successfully added a Course. ");
			
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.COMMIT );
		} 
		catch (Exception e)
		{
			daoFactory.deactivateConnection( DAO_Factory.TXN_STATUS.ROLLBACK );
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) 
	{
		daoFactory = new DAO_Factory();

		// menu
		int choice;
		Scanner scanner = new Scanner(System.in);
		
		getAllSkills();
		
		do
		{
			try{
				Runtime.getRuntime().exec("clear");
			}
			catch(Exception e){}

			System.out.println("\n\n\t\t\tNaukri.com");
		
			System.out.println("\n\n For Applicants");
			System.out.println(" 1. Create an Applicant profile");
			System.out.println(" 2. Browse Jobs");
			System.out.println(" 3. Apply for Job");
			System.out.println(" 4. Browse Courses");
			System.out.println(" 5. Enrol for Course");
			
			System.out.println("\n\n For Companies");
			System.out.println(" 6. Add Job Offer");
			System.out.println(" 7. Withdraw Job Offer");
			
			System.out.println("\n\n For Trainers");
			System.out.println(" 8. Offer a course");
			
			System.out.println("\n\n 0. Exit");
			
			System.out.print("\n\n Please enter your choice : ");
			choice = scanner.nextInt();
				

			switch(choice)
			{
				case 0:
					System.out.println("Exiting...");
				break;
				
				case 1:
				 	func1();
				break;

				case 2:
					func2();
				break;
				
				case 3:
				 	func3();
				break;

				case 4:
					func4();
				break;

				case 5:
				 	func5();
				break;

				case 6:
				 	func6();
				break;

				case 7:
				 	func7();
				break;

				case 8:
				 	func8();
				break;
				
				default:
					System.out.println(" Please enter a valid choice ... ");
				break;
			}
			
		} while(choice > 0);
		
	}
}
