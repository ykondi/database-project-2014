import java.lang.*;

public class Skill{
	int ID;
	String name;
	
	public Skill() { }

	public Skill(int _id, String _nm){
		ID = _id;
		name = _nm;
	}
	
	public String getName() { return name; }
	public void setName(String s){ name = s; }
	
	public int getID() { return ID; }
	public void setID(int s){ ID = s; }
	
	public void print()
	{ 
		System.out.println("\tSkill ID : " + ID); 
		System.out.println("\tName : " + name);
	}
};
