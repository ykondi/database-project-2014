// JobOffer.java

import java.lang.*;
import java.util.ArrayList;

public class JobOffer{
	int id;
	int basesalary;
	Company company;
	ArrayList<Skill> skills;

	public JobOffer(){}
	
	public JobOffer(int _id, int _basesalary, Company _company, ArrayList<Skill> _skills)
	{
		id = _id;
		basesalary = _basesalary;
		company = _company;
		skills = _skills;
	}

	public int getId() { return id; }
	public void setId(int _id) { id = _id; }
	
	public int getBaseSalary() { return basesalary; }
	public void setBaseSalary(int _basesalary) { basesalary = _basesalary; }
		
	public Company getCompany() { return company; }
	public void setCompany(Company _company) { company = _company; }

	public ArrayList<Skill> getSkills(){ return skills; }
	public void setSkills(ArrayList<Skill> _skills) { skills = _skills; }

	public void print() { 
		System.out.println("Joboffer Id: " + id); 
		System.out.println("Base salary: " + basesalary); 

		System.out.println("Company: "); 
		company.print();

		System.out.println("Skills: "); 
		for(Skill skill : skills){
			skill.print();
		}
	}
}
