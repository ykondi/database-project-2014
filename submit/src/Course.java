import java.lang.*;

public class Course{
	int ID;
	String name;
	Trainer trainer;
	Skill skill;
	
	public Course() { }
	
	public String getName() { return name; }
	public void setName(String s){ name = s; }
	
	public Skill getSkill() { return skill; }
	public void setSkill(Skill s){ skill = s; }
	
	public int getID() { return ID; }
	public void setID(int s){ ID = s; }
	
	public void setTrainer(Trainer t){ trainer = t; }
	public Trainer getTrainer(){ return trainer; }
	
	public void print()
	{ 
		System.out.println("Course ID : " + ID); 
		System.out.println("Name : " + name);
		
		System.out.println("Trainer: ");
			trainer.print();
		System.out.println("Skill: ");
			skill.print();
	}
};
