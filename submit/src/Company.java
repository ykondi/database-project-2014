import java.lang.*;

public class Company{
	int ID;
	String name;
	
	public Company() { }

	public Company(int _id, String _name) {
		ID = _id;
		name = _name;
	}
	
	public String getName() { return name; }
	public void setName(String s){ name = s; }
	
	public int getID() { return ID; }
	public void setID(int s){ ID = s; }
	
	public void print()
	{ 
		System.out.println("\tCompany ID = " + ID); 
		System.out.println("\tName = " + name);
	}
};
