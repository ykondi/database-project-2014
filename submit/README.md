2014 CC 105 Database Systems Lab Project
========================================

Authors
-------
* Ananth Murthy
* Chandan Yeshwanth
* Karthik S
* Yashvanth Kondi

File Descriptions
-----------------
* use_cases: List and descripton of use cases
* Relational Model.mwb: MySQL Workbench EER Model
* Relations.pdf: Description of Relations
* src/x.java Application class
* src/xDAO.java Data access objects
* src/xDAO_JDBC.java JDBC class
* sql/ SQL scripts to create tables, alter (add foreign keys), insert test data, drop foreigns keys and tables
* src/compile.sh Use to compile all Java source files

