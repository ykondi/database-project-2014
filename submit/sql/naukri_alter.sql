-- copied from generated .sql

ALTER TABLE course
	ADD CONSTRAINT `fk_Course_Trainer`
    FOREIGN KEY (`trainerid`)
    REFERENCES `trainer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE course
	ADD CONSTRAINT `fk_course_skill1`
    FOREIGN KEY (`skill`)
    REFERENCES `skill` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE joboffer
	ADD CONSTRAINT `fk_JobOffer_Company1`
    FOREIGN KEY (`companyid`)
    REFERENCES `company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_course
	ADD   CONSTRAINT `fk_Applicant_has_Course_Applicant1`
    FOREIGN KEY (`applicantid`)
    REFERENCES `applicant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_course
	ADD CONSTRAINT `fk_Applicant_has_Course_Course1`
    FOREIGN KEY (`courseid`)
    REFERENCES `course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_joboffer
	ADD  CONSTRAINT `fk_Applicant_has_JobOffer_Applicant1`
    FOREIGN KEY (`applicantid`)
    REFERENCES `applicant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_joboffer
	ADD CONSTRAINT `fk_Applicant_has_JobOffer_JobOffer1`
    FOREIGN KEY (`jobofferid`)
    REFERENCES `joboffer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_skill
	ADD CONSTRAINT `fk_Applicant_has_Skill_Applicant1`
    FOREIGN KEY (`applicantid`)
    REFERENCES `applicant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE applicant_skill    
	ADD CONSTRAINT `fk_Applicant_has_Skill_Skill1`
    FOREIGN KEY (`skillid`)
    REFERENCES `skill` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE joboffer_skill   
	ADD CONSTRAINT `fk_JobOffer_has_Skill_JobOffer1`
    FOREIGN KEY (`jobofferid`)
    REFERENCES `joboffer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;

ALTER TABLE joboffer_skill   
	ADD CONSTRAINT `fk_JobOffer_has_Skill_Skill1`
    FOREIGN KEY (`skillid`)
    REFERENCES `skill` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    ;
