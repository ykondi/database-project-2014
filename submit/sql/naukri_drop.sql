-- copied from generated .sql
-- drop FKs, then tables

ALTER TABLE course
	 DROP FOREIGN KEY `fk_Course_Trainer`;

ALTER TABLE course
	DROP FOREIGN KEY `fk_course_skill1`;
    
ALTER TABLE joboffer
	DROP FOREIGN KEY `fk_JobOffer_Company1`;

ALTER TABLE applicant_course
	DROP FOREIGN KEY `fk_Applicant_has_Course_Applicant1`;

ALTER TABLE applicant_course
	DROP FOREIGN KEY `fk_Applicant_has_Course_Course1`;

ALTER TABLE applicant_joboffer   
	DROP FOREIGN KEY `fk_Applicant_has_JobOffer_Applicant1`;

ALTER TABLE applicant_joboffer   
	DROP FOREIGN KEY `fk_Applicant_has_JobOffer_JobOffer1`;

ALTER TABLE applicant_skill   
	DROP FOREIGN KEY `fk_Applicant_has_Skill_Applicant1`;

ALTER TABLE applicant_skill   
	DROP FOREIGN KEY `fk_Applicant_has_Skill_Skill1`;

ALTER TABLE joboffer_skill   
	DROP FOREIGN KEY `fk_JobOffer_has_Skill_JobOffer1`;
    
ALTER TABLE joboffer_skill   
	DROP FOREIGN KEY `fk_JobOffer_has_Skill_Skill1`;

DROP TABLE IF EXISTS `trainer` ;
DROP TABLE IF EXISTS `skill` ;
DROP TABLE IF EXISTS `course` ;
DROP TABLE IF EXISTS `applicant` ;
DROP TABLE IF EXISTS `company` ;
DROP TABLE IF EXISTS `joboffer` ;
DROP TABLE IF EXISTS `applicant_course` ;
DROP TABLE IF EXISTS `applicant_joboffer` ;
DROP TABLE IF EXISTS `applicant_skill` ;
DROP TABLE IF EXISTS `joboffer_skill` ;